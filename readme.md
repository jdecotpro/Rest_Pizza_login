# titre

|Méthode   |URI   |Resultat   |Paramètres   |   |
|---|---|---|---|---|
|POST   |http://localhost:8080/myapp/pizzas   |Crée une pizza   |{"id": "5","nom": "Pizzarouge","base": "rouge"}   |
|GET   |http://localhost:8080/myapp/pizzas   |Affiche toutes les pizza   |
|DELETE   |http://localhost:8080/myapp/pizzas/3   |Supprime la pizza   |id de la pizza à supprimer   |
|GET  |http://localhost:8080/myapp/pizzas/1   |Affiche la pizza 1   |id de la pizza à afficher   |   |
|POST   |http://localhost:8080/myapp/commande   |Crée une commande   |{"idcommand" : "10","iduser": "2","idpizza": "5","prenom": "Julien"}   |   |
|GET   |http://localhost:8080/myapp/commande/   |Affiche toutes les commandes   |
|DELETE   |http://localhost:8080/myapp/commande/3   |Supprime la commande 3   |id de la commande à supprimer   |
|GET   |http://localhost:8080/myapp/commande/3   |Affiche la commande 3   |id de la commande à afficher   |