package fr.ulille.iut;

public class PizzaDto {
private String id;
private String nom;
private String base;

    public PizzaDto(String id, String nom, String base) {
        this.id = id;
        this.nom = nom;
        this.base = base;
    }

    public PizzaDto() {
    }

    public String getId() {
        return id;
    }
    public String getNom() {
        return nom;
    }
    public String getBase() {
        return base;
    }

    public void setId(String id){
        this.id = id;
    }
    public void setNom(String nom){
        this.nom = nom;
    }
    public void setBase(String base){
        this.base = base;
    }
    public String toString() {
        return "Id : " + id + ", nom : " + nom + ", base : " + base + ".";
    }
}