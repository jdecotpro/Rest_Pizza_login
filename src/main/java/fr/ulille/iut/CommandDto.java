package fr.ulille.iut;

public class CommandDto {
private String iduser;
private String idcommand;
private String prenom;
private String idpizza;

    public CommandDto(String idcommand, String iduser, String prenom, String idpizza) {
        this.idcommand = idcommand;
        this.iduser = iduser;
        this.prenom = prenom;
        this.idpizza = idpizza;
    }

    public CommandDto() {
    }

    public String getIdUser() {
        return iduser;
    }
    public String getIdCommand(){
        return idcommand;
    }
    public String getPrenom() {
        return prenom;
    }
    public String getIdPizza() {
        return idpizza;
    }

    public void setIdUser(String iduser){
        this.iduser = iduser;
    }
    public void setIdCommand(String idcommand){
        this.idcommand = idcommand;
    }
    public void setPrenom(String prenom){
        this.prenom = prenom;
    }
    public void setIdPizza(String idpizza){
        this.idpizza = idpizza;
    }
    public String toString() {
        return "IdCommand : " + idcommand + "IdUser : " + iduser + ", nom : " + prenom + ", base : " + idpizza + ".";
    }
}