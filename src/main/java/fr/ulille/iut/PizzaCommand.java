package fr.ulille.iut;

import javax.ws.rs.core.*;
import javax.ws.rs.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.Map;
import java.util.HashMap;

/**
 * Ressource des commandes (accessible avec le chemin "/commande")
 */
@Path("commande")
public class PizzaCommand {

    private static Map<String, CommandDto> commands = new HashMap<>();


    @Context
    public UriInfo uriInfo;

    public PizzaCommand() {
    }

    // **************** AFFICHE TOUTES LES COMMANDES ****************
    @GET
    public Map<String, CommandDto> getcommands() {
        return commands;
    }



 // RECUPERE LA COMMANDE EN FONCTION DE SON ID
    @GET
    @Path("{id}")
    public CommandDto getcommand(@PathParam("id") String id) {
        CommandDto commandError = new CommandDto("1001", "0","commande erreur", "0");
        int commandKey;
        int paramKey;
        paramKey = Integer.parseInt(id);
        for (Map.Entry<String, CommandDto> entry : commands.entrySet()) {
            commandKey = Integer.parseInt(entry.getKey());    
            if (commandKey == paramKey){
                CommandDto commandRetour = entry.getValue();
                return commandRetour;
            }
        }
        return commandError;
    }

    @DELETE
    @Path("{id}")
    public String deleteCommand(@PathParam("id") String id){
        int commandKey;
        int paramKey;
        String message;
        paramKey = Integer.parseInt(id);
        for (Map.Entry<String, CommandDto> entry : commands.entrySet()) {
            commandKey = Integer.parseInt(entry.getKey());    
            if (commandKey == paramKey){
                commands.remove(entry.getKey());
                message = "nous avons remove la commande " + paramKey;
                return message;
            }

        }
        message = "erreur de suppression de la commande";
        return message;
    }






    /*
    ************************* INSERE UNE COMMANDE ******************
    Selectionner POST
    Faire un custom header avec content-type et application-json
    ecrire ça : 
        {   
            "iduser": "2", 
            "idcommand" : "1",
            "prenom": "Julien", 
            "idpizza": "3"
        }
    et lancer la requete pour créer la commande
    */
    @POST
    public Response createCommand(CommandDto command) {
        if ( commands.containsKey(command.getIdCommand()) ) {
            return Response.status(Response.Status.CONFLICT).build();
        }
        else {
            commands.put(command.getIdCommand(), command);

            // On renvoie 201 et l'instance de la ressource dans le Header HTTP 'Location'
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path(command.getIdCommand()).build();
            return Response.created(instanceURI).build();
        }
    }


}