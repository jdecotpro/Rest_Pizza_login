package fr.ulille.iut;

import javax.ws.rs.core.*;
import javax.ws.rs.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.Map;
import java.util.HashMap;

/**
 * Ressource Pizza (accessible avec le chemin "/pizzas")
 */
@Path("pizzas")
public class PizzaResource {
    // Pour l'instant, on se contentera d'une variable statique pour conserver l'état
    private static Map<String, PizzaDto> pizzas = new HashMap<>();

    // L'annotation @Context permet de récupérer des informations sur le contexte d'exécution de la ressource.
    // Ici, on récupère les informations concernant l'URI de la requête HTTP, ce qui nous permettra de manipuler
    // les URI de manière générique.
    @Context
    public UriInfo uriInfo;

    /**
     * Une ressource doit avoir un contructeur (éventuellement sans arguments)
     */
    public PizzaResource() {
    }

    /**
     * Méthode de création d'un utilisateur qui prend en charge les requêtes HTTP POST
     * La méthode renvoie l'URI de la nouvelle instance en cas de succès
     *
     * @param  pizza Instance d'utilisateur à créer
     * @return Response le corps de la réponse est vide, le code de retour HTTP est fixé à 201 si la création est faite
     *         L'en-tête contient un champs Location avec l'URI de la nouvelle ressource
     */

    // AFFICHE TOUTES LES PIZZA
    @GET
    public Map<String, PizzaDto> getpizza() {
        return pizzas;
    }


    // RECUPERE LA PIZZA EN FONCTION DE SON ID
    @GET
    @Path("{id}")
    public PizzaDto getpizza(@PathParam("id") String id) {
        PizzaDto pizzaError = new PizzaDto("98", "ErrorPizza", "PimentRouge");
        int pizzaKey;
        int paramKey;
        paramKey = Integer.parseInt(id);
        for (Map.Entry<String, PizzaDto> entry : pizzas.entrySet()) {
            pizzaKey = Integer.parseInt(entry.getKey());    
            if (pizzaKey == paramKey){
                PizzaDto pizzaRetour = entry.getValue();
                return pizzaRetour;
            }
        }
        return pizzaError;
    }

    @DELETE
    @Path("{id}")
    public String deletePizza(@PathParam("id") String id){
        int pizzaKey;
        int paramKey;
        String message;
        paramKey = Integer.parseInt(id);
        for (Map.Entry<String, PizzaDto> entry : pizzas.entrySet()) {
            pizzaKey = Integer.parseInt(entry.getKey());    
            if (pizzaKey == paramKey){
                pizzas.remove(entry.getKey());
                message = "nous avons remove la pizza " + paramKey;
                return message;
            }

        }
        message = "erreur de suppression de la pizza";
        return message;
    }



    /*
    INSERE UNE PIZZA
    Selectionner POST
    Faire un custom header avec content-type et application-json
    ecrire ça : 
        {   
            "id": "3", 
            "nom": "PizzaTerre", 
            "base": "Terre"
        }
    et lancer la requete pour créer la pizza
    */
    @POST
    public Response createPizza(PizzaDto pizza) {
        // Si l'utilisateur existe déjà, renvoyer 409
        if ( pizzas.containsKey(pizza.getId()) ) {
            return Response.status(Response.Status.CONFLICT).build();
        }
        else {
            pizzas.put(""+ pizza.getId(), pizza);

            // On renvoie 201 et l'instance de la ressource dans le Header HTTP 'Location'
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();
            return Response.created(instanceURI).build();
        }
    }
}